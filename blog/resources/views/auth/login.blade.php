@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row ">

        @if(isset($info))
            {{dd($info)}}
            @foreach($info as  $data)
                {{dd($data)}}
                    <li>{{ $data->website_id }}</li>
                    <li>{{ $data->url }}</li>
                    <li>{{ $data->title }}</li>
            @endforeach
@endif
            @if($errors->any())

                <h4 class="alert alert-primary ">{{$errors->first()}}</h4>
            @endif
    </div>
</div>
@endsection
