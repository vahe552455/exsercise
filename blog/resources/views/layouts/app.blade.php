<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

        @section('header')
            <section class="navbar navbar-expand-lg navbar-dark bg-dark py-4">
                <a class="navbar-brand" href="#">Navbar</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{url('')}}">Home <span class="sr-only">(current)</span></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="">Messages</a>
                        </li>
                            @guest
                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" href=" ">{{ __('Register') }}</a>
                                @endif
                            </li>
                            @endguest
                    </ul>
                    {{--go to this page--}}
                    {{--get all pages content and title--}}
                    <form class="form-inline navbar-nav mr-2" method ='POST' action="{{route('search')}}">
                        <input class="form-control mr-sm-2" type="search" placeholder="Go here" aria-label="Search" name="page">
                        @csrf
                        <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Go</button>
                    </form>

                    {{--search--}}
                    {{-- Get links for a given phrase--}}
                  <form class="form-inline navbar-nav" method ='POST' action="{{route('getpages')}}">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="search">
                        @csrf
                        <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                    </form>


                    {{--<ul class="navbar-nav">
                        @guest
                        <div class="topnav ml-2">
                            <div class="login-container">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <input type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }} py-1" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus >
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                    <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }} py-1" name="password" placeholder="Password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>
                                </form>
                            </div>
                        </div>

                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
       document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                    </ul>
                    @endguest--}}
                </div>
            </section>
        @show
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
