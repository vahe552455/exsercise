<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    protected $fillable = [
        'page','scheme'
    ];
    public function Links() {
        return $this->hasMany('App\Link');
    }
}
