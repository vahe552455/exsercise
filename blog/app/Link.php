<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{

    protected $fillable = [
        'url','website_id','title','info'
    ];
    public function Website() {
        return $this->belongsTo('App\Website');
    }
}
