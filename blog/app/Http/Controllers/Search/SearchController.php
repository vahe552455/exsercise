<?php

namespace App\Http\Controllers\Search;
use App\Http\Middleware\RedirectIfAuthenticated;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Website;
use App\Link;
use Spatie\ArrayToXml\ArrayToXml;
class SearchController extends Controller
{

    public function curl($link) {
//        this will return link html code
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $str = curl_exec($curl);
        curl_close($curl);
        return $str;
    }

    function clean($text){
        $clean = html_entity_decode(trim(str_replace(';','-',preg_replace('/\s+/S', " ", strip_tags($text)))));// remove everything
        return $clean;
    }

    public function index(Request $request) {
        $url = parse_url($request->page);
        $str = $this->curl($request->page);
        if( $str && !Website::where('page',$url['host'])->get()->first()) {
            ini_set('max_execution_time', 900);
            $website = Website::create([
                'page'=>$url['host'],
                'scheme'=>$url['scheme'].'://'
            ]);
            $dom = new \DOMDocument();
            @$dom->loadHTML($str);

            $dom->getElementsByTagName("script");
            $script = $dom->getElementsByTagName("script") ;
            $style = $dom->getElementsByTagName("style") ;
            while ( $script->length) {
                $script->item(0)->parentNode->removeChild($script->item(0));
            }
            while ($style->length) {
                $style->item(0)->parentNode->removeChild($style->item(0));
            }
            //  get  tytle and cleaned body
            $titles =  $dom->getElementsByTagName('title')[0]->textContent;
            $body =  $this->clean($dom->getElementsByTagName('body')[0]->textContent);
            // save in xml
            if(!file_exists('xml')) {
                mkdir('xml');
            }
            $row = Link::create([
                'url'=>'',
                'website_id'=>$website->id,
                'title'=>$titles,
                'info'=>$body
            ]);
            $arr =$row->toArray();
            $arr['website'] = $row->website->page;
          unset( $arr['website_id']);
                //    get xml from array
            $result = ArrayToXml::convert($arr);
                //    save in public/xml
            file_put_contents("xml\\".$url['host'].".xml", $result);
            $links = $dom->getElementsByTagName('a');
                //Iterate over the extracted links and display their URLs
            foreach ($links as $link){
                //Extract and show the "href" attribute.
                $href = $link->getAttribute('href');
                $str1 = $this->curl($url['scheme'].'://'.$url['host'].$href);
                // Load all links and get info about all pages remove scripts and styles
                if($str1) {
                    @$dom->loadHTML($str1);

                    $script = $dom->getElementsByTagName("script");
                    $style = $dom->getElementsByTagName("style") ;
                    while (( $script->length)) {
                        $script->item(0)->parentNode->removeChild($script->item(0));
                    }
                    while ($style->length) {
                        $style->item(0)->parentNode->removeChild($style->item(0));
                    }
                    $titles =  $dom->getElementsByTagName('title')[0]->textContent;
                    $body =  $this->clean($dom->getElementsByTagName('body')[0]->textContent);
                    $row = Link::create([
                        'url'=>$href,
                        'website_id'=>$website->id,
                        'title'=>$titles,
                        'info'=>$body
                    ]);
                    // save xml
                    $arr =$row->toArray();
                    $arr['website'] = $row->website->page;
                    unset( $arr['website_id']);
                    $result = ArrayToXml::convert($arr);
                    file_put_contents("xml\\".$url['host'].$row->id.".xml", $result);
                }
            }
            return redirect()->back()->withErrors(['Check DB']);
        }
        else {
            return redirect()->back()->withErrors(['Wrong url or you already have url data']);
        }
    }
                    //    this function for finding url with given  phrase
    public function find(Request $request) {
        echo $request->search;
        $info = Link::where('title','like','%'.$request->search.'%')->orWhere('url','like','%'.$request->search.'%')->get();
        if($info->first()) {
            foreach ($info as $data) {
                    //  get  url
                echo '<pre>'.$data->website->scheme.$data->website->page.$data->url.'<pre/>';
            }
        }
        else {
            $info = Link::where('info','like','%'.$request->search.'%');
            foreach ($info as $data) {
                echo '<pre>'.$data->website->scheme.$data->website->page.$data->url.'<pre/>';
            }
        }
    }
}
